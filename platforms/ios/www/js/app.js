var client = fm.websync.client; // shortcut

        var channel = "/1000000000001$A12032C6-8869-478B-BF3C-876ADE64FEED";
        // var channel = "/1000000001027$E6903C00-293D-4B3E-B67B-371C5E21CFB5"
        var url = "https://changenotification-bta.ident.com/changenotification/websync.ashx";

        var client = new fm.websync.client(url);

        client.connect({
            onSuccess: function (args) {
                if (args.isReconnect) {
                    console.log('Reconnected to browser push server ' + url);
                }
                else {
                    console.log('Connected to browser push server ' + url);
                }
            },
            onFailure: function (args) {
                if (args.isReconnect) {
                    console.log('Could not reconnect: ' + args.getErrorMessage());
                }
                else {
                    console.log('Could not connect: ' + args.getErrorMessage());
                }
            },
            onStreamFailure: function (args) {
                if (args.willReconnect) {
                    console.log('Connection to server ' + url + ' lost, reconnecting... ' +  args.getErrorMessage());
                }
                else {
                    console.log('Connection to server ' + url + ' lost permanently! ' +  args.getErrorMessage());
                }
            }
            });

// "https://changenotification-bta.ident.com/changenotification/websync.ashx"
// "/1000000000001$A12032C6-8869-478B-BF3C-876ADE64FEED"

        client.subscribe({
            channel: channel,
            onSuccess: function (args) {
                if (args.isResubscribe) {
                    console.log('Resubscribed to browser push channel '+ channel);
                }
                else {
                    console.log('Subscribed to browser push channel '+ channel);
                }
            },
            onFailure: function (args) {
                if (args.isResubscribe) {
                    console.error('Could not resubscribe to browser push server: ' +  args.getErrorMessage());
                }
                else {
                    console.error('Could not subscribe to browser push server: ' +  args.getErrorMessage());
                }
            },
            onReceive: function (arg) {
                var data = arg.getData(),
                    metaData;

                if (data["DomainKey"] !== "0") {
                    return false;
                }
                metaData = JSON.parse(data["MetaData"]);


                var tpl = $("<div class='panel panel-info'><div class='panel-heading'><h3 class='panel-title'>" + 
                metaData.title + " for patinet " + metaData.patient.id + " " + data.Operation + "</h3></div><div class='panel-body'><div>From " + 
                "<span class='label label-success'>" + new Date(metaData.startDateTime).toLocaleString() + "</span>" + " to " + "<span class='label label-success'>" + new Date(metaData.endDateTime).toLocaleString() + "</span>" 
                + "</div><div>" + metaData.reason + "</div><div>" + metaData.other + "</div></div></div>");

                $('.notificationWrapper').append(tpl);

                setTimeout(function () {tpl.addClass("showMessage")}, 1000);
                tpl.on("click", function () {
                    $(this).removeClass("showMessage");
                    setTimeout(function () {this.remove();}.bind(this), 1500);
                })
                console.log(arg.getData())
            }
        });